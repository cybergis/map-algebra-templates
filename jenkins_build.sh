#!/bin/bash
cd $HOME
mkdir jenkins_build && cd jenkins_build
git clone http://www.bitbucket.org/cybergis/map-algebra.git
cd map-algebra/src
pwd
module load cuda && module load anaconda
make clean && make && make clean
module purge
cd ../../../
rm -rf jenkins_build
echo "Finished with build"
