module load libjpeg zlib proj4 geos filegdb hdf4 hdf5 netcdf4 gdal
module load anaconda
export GDAL_DATA=/sw/anaconda/share/gdal/
make
mpirun -np 5 ./test pitremoved.tif
