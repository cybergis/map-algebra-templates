#ifndef PARSER_CC
#define PARSER_CC
/** parser.cc: argument parsing functions using TCLAP API
 * Author: Sunwoo Kim <kim392@illinois.edu>
 * Date: 06/19/2015
 */
#include <string.h>
#include "parser.h"

void parseOptions(int argc, char** argv,  char* fn1, char* fn2, 
                    char* ofn)
{
	try {
	        CmdLine cmd(
                  "mapalg: aspect,diff,div,multiply,slope,sum,planc"
                  , ' ', "0.99" );
            // Define arguments
    	    UnlabeledValueArg<string> r1("raster1", "raster 1", true,
                                "default","string", cmd);
    	    UnlabeledValueArg<string> r2("raster2", "raster 2", true,
                                "default","string", cmd);
    	    ValueArg<string> out("o","out", "output raster", true,
                                "default","string", cmd);
            
            // Parse the command line.
        	cmd.parse(argc,argv);
            strcpy(fn1, r1.getValue().c_str());
            strcpy(fn2, r2.getValue().c_str());
            strcpy(ofn, out.getValue().c_str());
	} 
    catch ( ArgException& e ) { 
        cout << "ERROR: " << e.error() << " " << e.argId() << endl; 
    }
}



void parseOptions(int argc, char** argv,  char* fn1, char* fn2, 
                    char* fn3, char* fn4, char* ofn)
{
	try {
	        CmdLine cmd( "mapalg: planc", ' ', "0.99" );
            // Define arguments
    	    UnlabeledValueArg<string> r1("raster1", "raster 1", true,
                                "default","string", cmd);
    	    UnlabeledValueArg<string> r2("raster2", "raster 2", true,
                                "default","string", cmd);
    	    UnlabeledValueArg<string> r3("raster3", "raster 3", true,
                                "default","string", cmd);
    	    UnlabeledValueArg<string> r4("raster4", "raster 4", true,
                                "default","string", cmd);
    	    ValueArg<string> out("o","out", "output raster", true,
                                "default","string", cmd);
            
            // Parse the command line.
        	cmd.parse(argc,argv);
            strcpy(fn1, r1.getValue().c_str());
            strcpy(fn2, r2.getValue().c_str());
            strcpy(fn3, r3.getValue().c_str());
            strcpy(fn4, r4.getValue().c_str());
            strcpy(ofn, out.getValue().c_str());
	} 
    catch ( ArgException& e ) { 
        cout << "ERROR: " << e.error() << " " << e.argId() << endl; 
    }
}



void parseOptions(int argc, char** argv, char* fn1, char* fn2, 
                    char* fn3, char* fn4, char* fn5, char* ofn)
{
	try {
	        CmdLine cmd( "mapalg: meanc/profc", ' ', "0.99" );
            // Define arguments
    	    UnlabeledValueArg<string> r1("raster1", "raster 1", true,
                                "default","string", cmd);
    	    UnlabeledValueArg<string> r2("raster2", "raster 2", true,
                                "default","string", cmd);
    	    UnlabeledValueArg<string> r3("raster3", "raster 3", true,
                                "default","string", cmd);
    	    UnlabeledValueArg<string> r4("raster4", "raster 4", true,
                                "default","string", cmd);
    	    UnlabeledValueArg<string> r5("raster5", "raster 5", true,
                                "default","string", cmd);
    	    ValueArg<string> out("o","out", "output raster", true,
                                "default","string", cmd);
            
            // Parse the command line.
        	cmd.parse(argc,argv);
            strcpy(fn1, r1.getValue().c_str());
            strcpy(fn2, r2.getValue().c_str());
            strcpy(fn3, r3.getValue().c_str());
            strcpy(fn4, r4.getValue().c_str());
            strcpy(fn5, r5.getValue().c_str());
            strcpy(ofn, out.getValue().c_str());
	} 
    catch ( ArgException& e ) { 
        cout << "ERROR: " << e.error() << " " << e.argId() << endl; 
    }
}



void parseOptions(int argc, char** argv, char* fn, 
                    float &xp, float &yp, float &zp, char* ofn, int &np)
{
	try {
	        CmdLine cmd( "mapalg-dist-openmp", ' ', "0.99" );

            // Define arguments
    	    UnlabeledValueArg<string> r1("raster1", "raster 1", true,
                                "default","string", cmd);
    	    UnlabeledValueArg<float> f1("x", "x float", true,
                                0.0,"float", cmd);
    	    UnlabeledValueArg<float> f2("y", "y float", true,
                                0.0,"float", cmd);
    	    UnlabeledValueArg<float> f3("z", "z float", true,
                                0.0,"float", cmd);
    	    ValueArg<string> out("o","out", "output raster", true,
                                "default","string", cmd);
    	    ValueArg<int> i1("n","np", "number of processors", true,
                                0,"int", cmd);
	        
            // Parse the command line.
        	cmd.parse(argc,argv);
            strcpy(fn, r1.getValue().c_str());
            xp = f1.getValue();
            yp = f2.getValue();
            zp = f3.getValue();
            strcpy(ofn, out.getValue().c_str());
            np = i1.getValue();
	} 
    catch ( ArgException& e ) { 
        cout << "ERROR: " << e.error() << " " << e.argId() << endl; 
    }
}



void parseOptions(int argc, char** argv, char* fn, 
                    float &xp, float &yp, float &zp, char* ofn)
{
	try {
	        CmdLine cmd("mapalg-dist/mrmw/mrsw", ' ', "0.99" );

            // Define arguments
    	    UnlabeledValueArg<string> r1("raster1", "raster 1", true,
                                "default","string", cmd);
    	    UnlabeledValueArg<float> f1("x", "x float", true,
                                0.0,"float", cmd);
    	    UnlabeledValueArg<float> f2("y", "y float", true,
                                0.0,"float", cmd);
    	    UnlabeledValueArg<float> f3("z", "z float", true,
                                0.0,"float", cmd);
    	    ValueArg<string> out("o","out", "output raster", true,
                                "default","string", cmd);
	        
            // Parse the command line.
        	cmd.parse(argc,argv);
            strcpy(fn, r1.getValue().c_str());
            xp = f1.getValue();
            yp = f2.getValue();
            zp = f3.getValue();
            strcpy(ofn, out.getValue().c_str());
	} 
    catch ( ArgException& e ) { 
        cout << "ERROR: " << e.error() << " " << e.argId() << endl; 
    }
}

#endif
