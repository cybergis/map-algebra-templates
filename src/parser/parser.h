#ifndef PARSER_H
#define PARSER_H
/** parser.h: argument parsing functions
 * Author: Sunwoo Kim <kim392@illinois.edu>
 * Data: 6/19/2015
 */
#include "../tclap/CmdLine.h"
#include <iostream>
#include <string>

using namespace TCLAP;
using namespace std;

//mapalg-aspect/diff/...
void parseOptions(int argc, char** argv, char * fn1, char * fn2, 
                    char * ofn);
//mapalg-planc
void parseOptions(int argc, char** argv, char * fn1, char * fn2, 
                    string fn3, char * fn4, char * ofn);
//mapalg-meanc/profc
void parseOptions(int argc, char** argv, char * fn1, char * fn2, 
                    char * fn3, char * fn4, char * fn5, char* ofn);
//mapalg-dist/mrmw/mrsw
void parseOptions(int argc, char** argv, string& fn, 
                    float &xp, float &yp, float &zp, string& ofn);

void parseOptions(int argc, char** argv, char* fn, 
                    float &xp, float &yp, float &zp, char* ofn);
//mapalg-dist-openmp
void parseOptions(int argc, char** argv, char * fn, 
                    float &xp, float &yp, float &zp, char * ofn, int &np);

#endif
