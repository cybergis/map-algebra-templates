/**
  * Author: Garrett Nickel <gmnicke2@illinois.edu>
  * Date: 06/18/2015
  */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "util.h"
#include "data.h"
#include "gdal.h"
#include "cpl_conv.h"
#include "cpl_string.h"

#include <cuda.h>

// Checks for errors every CUDA call
__host__
inline void cudaCheckError(cudaError_t e) {
    if(e != cudaSuccess) {
        fprintf(stderr,"%s in %s at line %d\n",
                        cudaGetErrorString(e),
                        __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }
}

//Kernel function to sum rasters in parallel
__global__
void raster_sum(float * in_raster1, float * in_raster2, float * out_raster,
                int dim_x_in1, int dim_y_in1,
                int dim_x_in2, int dim_y_in2,
                int dim_x_out, int dim_y_out,
                double nodata1, double nodata2) {

    // Calculate the thread's index
    int x = threadIdx.x + (blockIdx.x * blockDim.x);
    int y = threadIdx.y + (blockIdx.y * blockDim.y);

    // Calculate what index of the raster the thread will perform on
    int raster_idx = x + (dim_x_out * y);

    // Want threads outside of bounds to exit early
    if(x >= dim_x_out ||
        y >= dim_y_out) 
        return;

    if(raster_idx >= dim_x_out * dim_y_out) return;

    double n1 = fabs((double)(in_raster1[raster_idx]) - nodata1);
    double n2 = fabs((double)(in_raster2[raster_idx]) - nodata2);

    // Make sure index is within raster size
    // And copy global data to local
    float in_pixel_1, in_pixel_2;
    if(x < dim_x_in1 &&
        y < dim_y_in1 &&
        n1 >= 0.001) {
        in_pixel_1 = in_raster1[raster_idx];
    } else {
        in_pixel_1 = 0;
    }

    if(x < dim_x_in2 &&
        y < dim_y_in2 &&
        n2 >= 0.001) {
        in_pixel_2 = in_raster2[raster_idx];
    } else {
        in_pixel_2 = 0;
    }    

    // Calculate and store the sum
    out_raster[raster_idx] = in_pixel_1 + in_pixel_2;
}

int main(int argc, char ** argv) {
    if(argc < 2 || argc > 4) {
        fprintf(stderr, "Usage: <executable> <input raster 1> <input raster 2> <output raster>\n");
        exit(1);
    } 
    double t0, t1, t2, t3, t4, t5, t6, t7, t8; // timing info

    t0 = get_timemark(); // Start Reading

    // Read raster names from command line
    char * infile1 = argv[1];
    char * infile2 = argv[2];
    char * outfile = argv[3];

    // Host will prepare data
    double georef[2][6]; // Georef data structure for each raster
    char prj[2][2048]; // Project wkt for each raster
    double nodata[2];
    int dim_x[] = {0,0}, dim_y[] = {0,0};
    float * h_raster1 = 0, * h_raster2 = 0; // Float array representation of rasters

    GDALDatasetH r_in1, r_in2;

    printf("----------------------\n");
    printf("----INPUT RASTER 1----\n");
    printf("----------------------\n");
 
    // Open first raster file
    r_in1 = raster_open(infile1, georef[0], prj[0], &nodata[0], 
                        &dim_x[0], &dim_y[0]);
    raster_info(r_in1, georef[0], prj[0], nodata[0], dim_x[0], dim_y[0]);

    // Declare and read into host's array for input raster 1
    h_raster1 = (float *) malloc(dim_x[0] * dim_y[0] * sizeof(float));
    raster_read(r_in1, h_raster1, 0, 0, dim_x[0], dim_y[0]);
    raster_close(r_in1);

    printf("----------------------\n");
    printf("----INPUT RASTER 2----\n");
    printf("----------------------\n");

    // Repeat for input raster 2
    r_in2 = raster_open(infile2, georef[1], prj[1], &nodata[1],
                        &dim_x[1], &dim_y[1]);
    raster_info(r_in2, georef[1], prj[1], nodata[1], dim_x[1], dim_y[1]);
    h_raster2 = (float *) malloc(dim_x[1] * dim_y[1] * sizeof(float));
    raster_read(r_in2, h_raster2, 0, 0, dim_x[1], dim_y[1]);
    raster_close(r_in2); 

    // Determine number of bytes for inputs and output
    size_t in1_bytes = dim_x[0] * dim_y[0] * sizeof(float);
    size_t in2_bytes = dim_x[1] * dim_y[1] * sizeof(float);

    // Determine dimension for output
    int dim_x_out = (dim_x[0] > dim_x[1]) ? dim_x[0] : dim_x[1];
    int dim_y_out = (dim_y[0] > dim_y[1]) ? dim_y[0] : dim_y[1];

    size_t out_bytes = dim_x_out * dim_y_out * sizeof(float);

    t1 = get_timemark(); // End reading, Start data distribution

    // Declare device raster arrays
    float * d_raster1, * d_raster2, * d_out_raster;

    #ifdef DEBUG
    fprintf(stderr,"Allocating %0.4f MBytes on Device Global Memory.\n",
                    (in1_bytes+in2_bytes+out_bytes)/1024.0/1024.0);
    #endif

    // Allocate device memory for the arrays (Must be < ~10 GB)
    cudaCheckError( cudaMalloc((void **) &d_raster1, in1_bytes) );

    t2 = get_timemark(); // End timing first call w/ device init

    cudaCheckError( cudaMalloc((void **) &d_raster2, in2_bytes) );
    cudaCheckError( cudaMalloc((void **) &d_out_raster, out_bytes) );

    // Copy input arrays to device global memory
    cudaCheckError( cudaMemcpy(d_raster1, h_raster1, 
                                in1_bytes, cudaMemcpyHostToDevice) );
    cudaCheckError( cudaMemcpy(d_raster2, h_raster2,
                                in2_bytes, cudaMemcpyHostToDevice) );

    t3 = get_timemark(); // End data distr, start calculation

    // Create 2D grid of blocks with 2D thread blocks
    // Thread block is a 2D block of 1024 threads
    dim3 block_dim(32,32,1);

    // Want block grid to fit size of biggest raster,
    // If dim % 32 != 0, round up
    dim3 grid_dim((dim_x_out - 1) / 32 + 1,
                    (dim_y_out - 1) / 32 + 1, 1);

    /*void raster_sum(float * in_raster1, float * in_raster2, float * outraster,
                int dim_x_in1, int dim_y_in1,
                int dim_x_in2, int dim_y_in2,
                int dim_x_out, int dim_y_out) {
    */

    // Launch Kernel to perform sum in parallel on GPU
    raster_sum<<<grid_dim, block_dim>>>(d_raster1, d_raster2, d_out_raster,
                                        dim_x[0], dim_y[0],
                                        dim_x[1], dim_y[1],
                                        dim_x_out, dim_y_out,
                                        nodata[0], nodata[1]);

    // Wait for last kernel call to finish for correct timemark
    cudaDeviceSynchronize();

    t4 = get_timemark(); // End calculation, start data retrieve

    // Retrieve result raster array from device
    float * h_out_raster = (float *) malloc(out_bytes);
    cudaCheckError( cudaMemcpy(h_out_raster, d_out_raster,
                                out_bytes, cudaMemcpyDeviceToHost) );

    t5 = get_timemark(); // End data retrieve, start write

    // Write output
    GDALDatasetH r_out;
    printf("----------------------\n");
    printf("----OUTPUT RASTER-----\n");
    printf("----------------------\n");
 
    r_out = raster_create(outfile, dim_x_out, dim_y_out,
                            georef[0], prj[0], nodata[0]);
    raster_info(r_out, georef[0], prj[0], nodata[0], dim_x_out, dim_y_out);
    raster_write(r_out, h_out_raster, 0, 0, dim_x_out, dim_y_out);
    raster_close(r_out);

    t6 = get_timemark(); // End write

    // Free device's raster arrays
    cudaCheckError( cudaFree((void*) d_out_raster) );
    cudaCheckError( cudaFree((void*) d_raster1) );
    cudaCheckError( cudaFree((void*) d_raster2) );

    // Free host raster arrays
    free(h_raster1);
    free(h_raster2);
    free(h_out_raster);

    // Re-perform first cuda call to get a time w/o device init
    d_raster1 = NULL;
    t7 = get_timemark();
    cudaCheckError( cudaMalloc((void **) &d_raster1, in1_bytes) );
    t8 = get_timemark();
    cudaCheckError( cudaFree((void*) d_raster1 ) );

    jobstat.Tread = t1 - t0;
    jobstat.Tcommdata = t3 - t1; // Includes time for device to initialize
    jobstat.Tcommdata_nd = (t3 - t2) + (t8 - t7); //Roughly excludes init time
    jobstat.Tcompute = t4 - t3;
    jobstat.Tcommresult = t5 - t4;
    jobstat.Twrite = t6 - t5;
    jobstat.Ttotal = t6 - t0;
    // Total without time for device to init
    jobstat.Ttotal_nd = jobstat.Ttotal - jobstat.Tcommdata + jobstat.Tcommdata_nd;
    print_jobstat_cuda();

    cudaDeviceReset();

    return 0;
}
