/**
  * Author: Garrett Nickel <gmnicke2@illinois.edu>
  * Date: 06/22/2015
  *
  * Command Line Parsing Authors: Mingze Gao & Yiming Huang
  */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <iostream>
#include <vector>
#include <string>
#include <sstream>

#include "util.h"
#include "data.h"
#include "gdal.h"
#include "cpl_conv.h"
#include "cpl_string.h"

#include <cuda.h>

#define MAX_NAME_LEN 200

__host__
inline void cudaCheckError(cudaError_t e) {
    if(e != cudaSuccess) {
        fprintf(stderr, "%s in %s at line %d\n",
                        cudaGetErrorString(e),
                        __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }
}

__global__
void kernel_convolution(float * in_raster, float * out_raster, const float * kernel, 
                        const int kernel_width, const int kernel_height,
                        const int dim_x, const int dim_y) {


    // Find position of thread in grid
    const int2 thread_pos_grid = make_int2(blockIdx.x * blockDim.x + threadIdx.x,
                                        blockIdx.y * blockDim.y + threadIdx.y);

    // Find absolute position of thread on raster
    const int raster_idx = thread_pos_grid.x + (thread_pos_grid.y * dim_x);

    //Find absolute position of thread in its block
    const int thread_block_idx = threadIdx.x + (threadIdx.y * blockDim.x);

    // Check if index is within raster bounds
    if(thread_pos_grid.x >= dim_x ||
        thread_pos_grid.y >= dim_y) {
        return;
    }

    // Will have kernel be in shared memory for faster calculation
    // (Only if kernel area < 1025)
    extern __shared__ float shared_block[];

    // If kernel area is 1024 or less, can be on shared memory
    unsigned char using_shared_kernel = 0;
    const int kernel_area = kernel_width * kernel_height;

    if(kernel_area <= 1024 && thread_block_idx < kernel_area) {
        shared_block[thread_block_idx] = kernel[thread_block_idx];
        using_shared_kernel = 1;
    }

    // Wait for threads to finish copying kernel to shared
    __syncthreads();

    float result_val = 0.f;
   
    // Calculate value based on neighbor values * kernel weights
    int kernel_idx = -1;
    for(int ky = (-kernel_height/2); ky <= (kernel_height/2); ky++) {
        for(int kx = (-kernel_width/2); kx <= (kernel_width/2); kx++) {
            
            kernel_idx++;

            // Find index of neighbor
            const int neighbor_x = thread_pos_grid.x + kx;
            const int neighbor_y = thread_pos_grid.y + ky;
            const int neighbor_idx = neighbor_x + (neighbor_y * dim_x);

            // Make sure neighbor is within bounds
            if(neighbor_x < dim_x && neighbor_y < dim_y &&
                neighbor_x >= 0 && neighbor_y >= 0) {

                float neighbor_val = in_raster[neighbor_idx];

                float kernel_val;

                if(!using_shared_kernel) {
                    kernel_val = kernel[kernel_idx];
                } else {
                    // Retrieve the kernel data which is after raster data
                    kernel_val = shared_block[kernel_idx];                    
                }
                // Acculumate the neighbor values * weight values
                result_val += neighbor_val * kernel_val;

            }
        }
    }

    out_raster[raster_idx] = result_val;
}

int main(int argc, char * argv[]) {
    double t0, t1, t2, t3, t4, t5, t6, t7, t8; // Timing info

    char * input_file_name = (char *)malloc(sizeof(char) * MAX_NAME_LEN);
    char * output_file_name = (char *)malloc(sizeof(char) * MAX_NAME_LEN);
    char * kernel_file_name = (char *)malloc(sizeof(char) * MAX_NAME_LEN);
    int k_x, k_y; // Kernel dimensions
    int bandInd = -1;

    parseArg(argc, argv, input_file_name,
            output_file_name, kernel_file_name,
            k_x, k_y, bandInd);

    float * h_kernel = (float*)malloc(sizeof(float)*k_x*k_y);
    h_kernel = NULL;
    kernelReader(kernel_file_name,k_x,k_y,h_kernel);

    t0 = get_timemark(); // Start Reading
    
    // Host will prepare data
    double georef[6]; // Georef data structure for each raster
    char prj[2048]; // Project wkt for each raster
    double nodata;
    int dim_x = 0, dim_y = 0;
    float * h_raster = 0; // Float array representation of raster

    GDALDatasetH r_in, r_out;

    // Open raster file
    r_in = raster_open(input_file_name, georef, prj, &nodata,
                        &dim_x, &dim_y, bandInd);

    #ifdef DEBUG
    printf("-------------------------\n");
    printf("------INPUT RASTER-------\n");
    printf("-------------------------\n");
    raster_info(r_in, georef, prj, nodata, dim_x, dim_y);
    #endif

    // Determine size of raster and kernel arrays
    size_t r_bytes = dim_x * dim_y * sizeof(float);
    size_t k_bytes = k_x * k_y * sizeof(float);

    // Declare and read into host's array for input raster
    h_raster = (float *) malloc(r_bytes);

    raster_read(r_in, h_raster, 0, 0, dim_x, dim_y);
    raster_close(r_in);

    #ifdef DEBUG
    fprintf(stderr,"---FIRST 9x9 BLOCK OF INPUT---\n");
    for(int y = 0; y < 9; y++) {
        for(int x = 0; x < 9; x++) {
            int idx = x + (y * dim_x);
            fprintf(stderr, "%0.5f ", h_raster[idx]);
        }
        fprintf(stderr, "\n");
    }
    fprintf(stderr,"---9x9 BLOCK OF INPUT CROSSING THREAD BLOCK BOUNDARIES---\n");
    for(int y = 1020; y < 1029; y++) {
        for(int x = 1020; x < 1029; x++) {
            int idx = x + (y * dim_x);
            fprintf(stderr, "%0.5f ", h_raster[idx]);
        }
        fprintf(stderr, "\n");
    }
    #endif

    t1 = get_timemark(); // End reading, Start data distribution

    // Declare device raster and kernel arrays
    float * d_in_raster, * d_out_raster, * d_kernel;
    
    // Allocate device memory for the inout array (Must be < ~10 GB)
    cudaCheckError( cudaMalloc((void **) &d_in_raster, r_bytes) );

    t2 = get_timemark(); // End timing first call w/ device init

    // Allocate device memory for the kernel array
    cudaCheckError( cudaMalloc((void **) &d_kernel, k_bytes) );
    cudaCheckError( cudaMalloc((void **) &d_out_raster, r_bytes) );

    #ifdef DEBUG
    fprintf(stderr,"Allocating %0.4f MBytes on Device Global Memory.\n",
                    ((r_bytes + k_bytes) / 1024.0 / 1024.0));
    #endif

    // Copy raster and kernel to device global memory
    cudaCheckError( cudaMemcpy(d_in_raster, h_raster,
                                r_bytes, cudaMemcpyHostToDevice) );
    cudaCheckError( cudaMemcpy(d_kernel, h_kernel,
                                k_bytes, cudaMemcpyHostToDevice) );
    cudaCheckError( cudaMemset(d_out_raster, 0, r_bytes) );
    
    t3 = get_timemark(); // End data distr, start calculation

    // Create 2D grid of blocks with 2D thread blocks
    // Thread block is a 2D block of 1024 threads
    dim3 block_dim(32,32,1);

    // Want block grid to fit size of raster,
    // If dim % 32 != 0, round up
    dim3 grid_dim((dim_x - 1) / 32 + 1,
                    (dim_y - 1) / 32 + 1, 1);

    /*void kernel_convolution(float * inout_raster, const float * kernel, 
                        const int kernel_width, const int kernel_height,
                        const int dim_x, const int dim_y) {
    */

    int kernel_area = k_x * k_y;
    // If a whole block can see a kernel, it will be on shared mem
    size_t shared_bytes;
    if(kernel_area <= 1024) {
        shared_bytes = k_bytes;
    } else {
        shared_bytes = 1;
    }
    
    // Compute kernel convolution in parallel on GPU
    kernel_convolution<<<grid_dim, block_dim, shared_bytes>>>(d_in_raster,
                                                            d_out_raster,
                                                            d_kernel, k_x, k_y,
                                                            dim_x, dim_y);

    // Wait for all threads to finish
    cudaDeviceSynchronize();

    t4 = get_timemark(); // End calculation, start data retrieve

    // Retrieve result raster array from device (don't need kernel back)
    cudaCheckError( cudaMemcpy(h_raster, d_out_raster,
                                r_bytes, cudaMemcpyDeviceToHost) );

    t5 = get_timemark(); // End data retrieve, start write

    // Write output
    r_out = raster_create(output_file_name, 
                            dim_x, dim_y,
                            georef, prj, nodata);
    
    #ifdef DEBUG
    printf("-------------------------\n");
    printf("------OUTPUT RASTER------\n");
    printf("-------------------------\n");
    raster_info(r_out, georef, prj, nodata, dim_x, dim_y);
    #endif

    raster_write(r_out, h_raster, 0, 0, dim_x, dim_y);
    raster_close(r_out);

    t6 = get_timemark(); // End write

    #ifdef DEBUG
    fprintf(stderr,"---FIRST 9x9 BLOCK OF OUTPUT---\n");
    for(int y = 0; y < 9; y++) {
        for(int x = 0; x < 9; x++) {
            int idx = x + (y * dim_x);
            fprintf(stderr, "%0.5f ", h_raster[idx]);
        }
        fprintf(stderr, "\n");
    }
    fprintf(stderr,"---9x9 BLOCK OF OUTPUT CROSSING THREAD BLOCK BOUNDARIES---\n");
    for(int y = 1020; y < 1029; y++) {
        for(int x = 1020; x < 1029; x++) {
            int idx = x + (y * dim_x);
            fprintf(stderr, "%0.5f ", h_raster[idx]);
        }
        fprintf(stderr, "\n");
    }
    #endif

    // Clean up
    cudaCheckError( cudaFree((void *) d_in_raster) );
    cudaCheckError( cudaFree((void *) d_out_raster) );
    cudaCheckError( cudaFree((void *) d_kernel) );
    free(h_raster);

    // Re-perform first cuda call to get a time w/o device init
    d_in_raster = 0;
    t7 = get_timemark();
    cudaCheckError( cudaMalloc((void**) &d_in_raster, r_bytes) );
    t8 = get_timemark();
    cudaDeviceSynchronize();
    cudaCheckError( cudaFree((void*) d_in_raster) );

    jobstat.Tread = t1 - t0;
    jobstat.Tcommdata = t3 - t1; // Includes time for device to initialize
    jobstat.Tcommdata_nd = (t3 - t2) + (t8 - t7); // Roughly excludes init time
    jobstat.Tcompute = t4 - t3;
    jobstat.Tcommresult = t5 - t4;
    jobstat.Twrite = t6 - t5;
    jobstat.Ttotal = t6 - t0;
    // Total without time for device to init
    jobstat.Ttotal_nd = jobstat.Ttotal - jobstat.Tcommdata + jobstat.Tcommdata_nd;
    print_jobstat_cuda();

    // Resetting is recommended
    cudaDeviceReset();

    return 0;
}
